fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json) => console.log(json));


//map- no answer still confuse
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json) => {
	let list = json.map((todos=> {
		return todos.title;
	}))
	console.log(list);
});


fetch("https://jsonplaceholder.typicode.com/todos/2")
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/2")
.then((response) => response.json())
.then((json) => console.log(json.title));
 
fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		completed: false,
		userId: 1,
		title: "Created to do List Items",
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "Updated To Do List Item",
		status: "Pending",
		description: "To updated my to do list with a different data structure.",
		dateCompleted: "Pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		userId: 1,
		title: "delectus aut autem",
		status: "Completed",
		dateCompleted: "07/09/21",
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});
