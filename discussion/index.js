// JS Synchronous vs JS Asynchronous
// JS Synchronous programming - only one statement/line of codes is being processed at a time; being used by javascript by default

// the error checking proves the synchronous programming of javascript since the after detecting the error, the next lines of codes will not be executed even if they have no errors
console.log("Hello World");
// conosle.log("Hello Again");
console.log("Goodbye");

// when certain statements take a lot of time to process, this slows down the running/executing of codes.
// an example of this is when loops are used on a large amount of information or when fetching data from database
// when an action will take some time to be executed, this results in "code blocking"
	// code blocking - delaying of a more efficient code compared to ones currently executed.
console.log("Hello World");
// we might not notice it due to improved processing power of our devices, but the process of fetching/using large amounts of information in our loops takes too much compared to logging "Hello Again"
// another example is when you try to access a website and it takes a while to load (white webpage is being displayed before the landing page is loaded)
/*for (let i = 0; i <= 1500; i++) {
	console.log(i);
};*/
console.log("Hello Again");


/*
JAVASCRIPT ASYNCHRONOUS CODE
// Asynchronous JS
// Asynchronous - executing at the sametime
	//most asynch js operations can be classified through:
		// Browser/WEB API events or functions; include methods like setTimeout, or event handlers like onclick, mouse over scroll, etc/; thus, asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background
		// it prioritizes the most efficient codes to run while also performing the less efficient ones in the background
*/

// FETCH function Fetch API - allows us to asynchronously fetch/request for a resource (data)
// a "promise" is an object that represents eventual completion (or failure) of an asynchronous function and its resulting
// SYNTAX:
/*
	fetch('URL')
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));




/*
SYNTAX:
	fetch(url).then((parameter) => statement).then((parameter) => statement)
*/
// retrieves all posts following the REST API method (read/GET)
// by using the .then method, we can now check the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")
// since fetch method returns a "promise", the "then" method will catch the promise and make it the "response" object
// also, the then method captures the "response" object and returns another promise which will eventually be rejected/resolved
.then(response => console.log(response));

// this will be logged first, before the status of the promise due to javascript asychronous 
console.log("Hello Again");

fetch("https://jsonplaceholder.typicode.com/posts")
// the use of "json()" is to convert the response object into json format to be used by the application
.then((response)=> response.json())
// since we cannot directly print the json format of the response in the second .then method, we need another .then method to catch the promise and print the "response.json()" which is being represented by the "json" parameter
// using them methods multiple times would create promise chains
.then((json) => console.log(json));


// ASYNC-AWAIT
// async-await keywords is another approach that can be used to achieve js asynchronous
// used in functions to indicate which portions of code should be waited
// the codes outside the functions will be executed under JS asynchronous
async function fetchData() {
	// waits for the fetch method to be done before storing value of the response in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	// returns a promise
	console.log(result);
	// returns the type of data the "result" variable has
	console.log(typeof result);
	// we cannot access the body of the result
	console.log(result.body);
	// converts the data from the "result" variable into json and stores it in "json" variable
	let json = await result.json();
	console.log(json);
}
fetchData();
console.log("Hello Again");

/*
miniactivity
using the then method, retrieve the first object in the jsonplaceholder url.
	the response should be converted first into json format before being displayed in the console
*/
// retrieves a specific object using the id (landingUrl.com/posts/:id, GET method)
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));


// CREATE/UPDATE A RESOURCE
/*
	SYNTAX
	fetch ("url", {options}, details of the request body)
	.then(response => {})
	.then((json) => {})
*/
// using post method to create object (posts/:id, POST)
fetch("https://jsonplaceholder.typicode.com/posts",{
	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	method: "POST",
	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		"Content-Type": "application/json"
	},
	// Sets the content/body data of the "Request" object 
	// JSON.stringify converts the object data into a stringified JSON
	body:JSON.stringify({
		userId: 1,
		title: "New Post",
		body:"Hello World"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



// updating a resource

/*
	PUT - replaces the whole object
	PATCH - updates the specified key/s

*/
/*
	create another fetch request (the url should contain 1 as the id endpoint) with PUT method
		just only specify the title
			title: corrected post
*/

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "Corrected Post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
create another fetch request (the url should contain 1 as the id endpoint) with PATCH method
	userId: 1
	title: Updated Post
*/
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "Updated Post",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// deleting a resource
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})

/*
Filtering posts
*/
/*
	the data/result coming from the fetch method can be filtered by sending a key-value pair along with its URL

	the information is sent via the url can be done by adding the question mark symbol (?)

	SYNTAX
		-"url?parameterName=value"
		-"url?paramA=valueA&paramB=valueB"
		*/

// filtering using single parameter
// fetch('https://jsonplaceholder.typicode.com/posts?userId=1')

//filtering using multiple parameter 
fetch('https://jsonplaceholder.typicode.com/posts?userId=1&id=3')
.then((response) => response.json())
.then((json) => console.log(json));

/*
retrieve the nested comments array in the first entry using get method
*/
// Retrieving nested/related comments to posts
// GET Method url/posts/:id/comments
fetch("https://jsonplaceholder.typicode.com/posts/?userId=1/comments")
.then((response)=>response.json())
.then((json)=>console.log(json));
